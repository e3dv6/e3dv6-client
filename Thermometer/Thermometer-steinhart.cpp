/*#include "Thermometer.h"
#include "arduino.h"
#include "../Utils/Log.h"

// which analog pin to connect
#define THERMISTORPIN A0
// resistance at 25 degrees C
#define THERMISTORNOMINAL 100000
// temp. for nominal resistance (almost always 25 C)
#define TEMPERATURENOMINAL 25
// how many samples to take and average, more takes longer
// but is more 'smooth'
#define NUMSAMPLES 32
// The beta coefficient of the thermistor (usually 3000-4000)
#define BCOEFFICIENT 4267
// the value of the 'other' resistor
#define SERIESRESISTOR 100000

int samples[NUMSAMPLES];

float Thermometer::getTemperature() {
  analogReference(EXTERNAL);

  uint8_t i;
  float average;

  // take N samples in a row, with a slight delay
  for (i = 0; i < NUMSAMPLES; i++) {
    samples[i] = analogRead(THERMISTORPIN);
    delay(10);
  }


    // average all the samples out
    average = 0;
    for (i=0; i< NUMSAMPLES; i++) {
       average += samples[i];
    }
    average /= NUMSAMPLES;

    Log::debug("Average analog reading ");
    Log::debug(average);

    // convert the value to resistance
    average = 1023 / average - 1;
    average = SERIESRESISTOR / average;
    Log::debug("Thermistor resistance ");
    Log::debug(average);

    float steinhart;
    steinhart = average / THERMISTORNOMINAL;     // (R/Ro)


  Thermometer::isort(samples, NUMSAMPLES);

  float value = samples[16];

  Log::debug("Median analog reading ");
  Log::debug(value);

  // convert the value to resistance
  value = 1023 / value - 1;
  value = SERIESRESISTOR / value;
  Log::debug("Thermistor resistance ");
  Log::debug(value);

  float steinhart;
  steinhart = value / THERMISTORNOMINAL;     // (R/Ro)
  steinhart = log(steinhart);                  // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  steinhart = 1.0 / steinhart;                 // Invert
  steinhart -= 273.15;                         // convert to C

  Log::debug("Temperature ");
  Log::debug(steinhart);
  Log::debug(" *C");
  return steinhart;
}

void Thermometer::isort(int *a, int n) {
  for (int i = 1; i < n; ++i)
  {
    int j = a[i];
    int k;
    for (k = i - 1; (k >= 0) && (j < a[k]); k--)
    {
      a[k + 1] = a[k];
    }
    a[k + 1] = j;
  }
}
*/