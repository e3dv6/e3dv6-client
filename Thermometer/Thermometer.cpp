#include "Thermometer.h"
#include "arduino.h"
#include "../Utils/Log.h"

const short temperatureTable[][2] = {
  {1, 713},
  {17, 300}, //top rating 300C
  {20, 290},
  {23, 280},
  {27, 270},
  {31, 260},
  {37, 250},
  {43, 240},
  {51, 230},
  {61, 220},
  {73, 210},
  {87, 200},
  {106, 190},
  {128, 180},
  {155, 170},
  {189, 160},
  {230, 150},
  {278, 140},
  {336, 130},
  {402, 120},
  {476, 110},
  {554, 100},
  {635, 90},
  {713, 80},
  {784, 70},
  {846, 60},
  {897, 50},
  {937, 40},
  {966, 30},
  {986, 20},
  {1000, 10},
  {1010, 0}
};

// which analog pin to connect
#define THERMISTORPIN A0
#define NUMSAMPLES 32

// resistance at 25 degrees C
#define THERMISTORNOMINAL 100000
// temp. for nominal resistance (almost always 25 C)
#define TEMPERATURENOMINAL 25
// The beta coefficient of the thermistor (usually 3000-4000)
#define BCOEFFICIENT 4267
// the value of the 'other' resistor
#define SERIESRESISTOR 100000

int samples[NUMSAMPLES];

float Thermometer::getTemperature() {
  Log::debug("*************************");
  analogReference(EXTERNAL);

  uint8_t i;

  // take N samples in a row, with a slight delay
  for (i = 0; i < NUMSAMPLES; i++) {
    samples[i] = analogRead(THERMISTORPIN);
    delay(10);
  }

  Thermometer::isort(samples, NUMSAMPLES);

  float adc = samples[16];

  Log::debug("Median analog reading ");
  Log::debug(adc);

  /* REMOVE */

  // convert the value to resistance
  float value;
  value = 1023 / adc - 1;
  value = SERIESRESISTOR / value;
  Log::debug("Thermistor resistance ");
  Log::debug(value);

  float steinhart;
  steinhart = value / THERMISTORNOMINAL;     // (R/Ro)
  steinhart = log(steinhart);                  // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  steinhart = 1.0 / steinhart;                 // Invert
  steinhart -= 273.15;                         // convert to C

  Log::debug("Temperature (steinhart) ");
  Log::debug(steinhart);
  Log::debug(" *C");

  /* /REMOVE */

  int rows = sizeof(temperatureTable) / sizeof(temperatureTable[0]);
  float celsius = 0;

  for (i = 1; i < rows; i++) {
    if (temperatureTable[i][0] > adc) {
      celsius = temperatureTable[i - 1][1] +
                (adc - temperatureTable[i - 1][0]) *
                (float)(temperatureTable[i][1] - temperatureTable[i - 1][1]) /
                (float)(temperatureTable[i][0] - temperatureTable[i - 1][0]);
      break;
    }
  }

  // If we received a adc value that is larger than any value in our temperature table, then return the last value in the table.
  if (i == rows) {
    celsius = temperatureTable[i - 1][1];
  }

  Log::debug("Temperature (table) ");
  Log::debug(celsius);
  Log::debug(" *C");

  return celsius;
}

void Thermometer::isort(int *a, int n) {
  for (int i = 1; i < n; ++i)
  {
    int j = a[i];
    int k;
    for (k = i - 1; (k >= 0) && (j < a[k]); k--)
    {
      a[k + 1] = a[k];
    }
    a[k + 1] = j;
  }
}
