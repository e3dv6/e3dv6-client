#ifndef Thermometer_h
#define Thermometer_h

class Thermometer {

  public:
    static float getTemperature();

  private:
    static void isort(int *a, int n);
};

#endif
