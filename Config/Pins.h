#ifndef Pins_h
#define Pins_h

enum Pins {
  HEATER = 8, // PWM Heater
  FAN_1 = 7,   // PWM Fan
  FAN_2 = 6,   // PWM Fan
  FAN_3 = 5    // Relay Fan
};

#endif
