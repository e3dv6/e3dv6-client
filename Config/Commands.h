#ifndef Commands_h
#define Commands_h

enum Commands {
  PING = 'a',
  GET_CURRENT_TEMPERATURE = 'b',
  GET_TARGET_TEMPERATURE = 'c',
  SET_TARGET_TEMPERATURE = 'd',
  IS_HEATER_ON = 'e',
  GET_HEATER_PERCENTAGE = 'f',
  SET_FAN1_SPEED = 'g',
  GET_FAN1_SPEED = 'h',
  SET_FAN2_SPEED = 'i',
  GET_FAN2_SPEED = 'j',
  SET_FAN3_SPEED = 'k',
  GET_FAN3_SPEED = 'l',
  SET_HEATER_DECAY = 'm',
  SET_HEATER_INCREASE = 'n',
  SET_HEATER_THERMAL_RESISTANCE = 'o'
};

#endif
