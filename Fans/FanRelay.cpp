#include "FanRelay.h"
#include "../Utils/Log.h"

FanRelay::FanRelay(uint8_t _pin) {
  pin = _pin;
  //pinMode(pin, OUTPUT);
  isActive = false;
}

int FanRelay::getSpeed() {
  Log::debug("Getting Fan Speed");

  if (isActive) {
    return 100;
  } else {
    return 0;
  }
}

void FanRelay::setSpeed(int _percentage) {
  Log::debug("Setting Fan Speed");

  if (_percentage > 0) {
    isActive = true;
    //digitalWrite(pin, HIGH);
  } else {
    isActive = false;
    //digitalWrite(pin, LOW);
  }
}

