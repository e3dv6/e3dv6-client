#include "FanPWM.h"
#include "../Utils/Log.h"

FanPWM::FanPWM(uint8_t _pin) {
  pin = _pin;
  //pinMode(_pin, OUTPUT);
  percentage = 0;
}

int FanPWM::getSpeed() {
  Log::debug("Getting Fan Speed");
  return percentage;
}

void FanPWM::setSpeed(int _percentage) {
  Log::debug("Setting Fan Speed To: ");
  Log::debug(_percentage);

  if (_percentage >= 0 && _percentage <= 100) {
    percentage = _percentage;
    //analogWrite(pin, 255.0 * (percentage / 100.0));
  } else {
    Log::debug("Invalid fan speed percentage, expected fan speed to be between 0 and 100.");
  }
}

