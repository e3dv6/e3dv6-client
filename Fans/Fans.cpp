#include "Fans.h"
#include "../Utils/Log.h"
#include "IFan.h"
#include "FanPWM.h"
#include "FanRelay.h"
#include "../Config/Pins.h"

IFan *Fans::fans[3];
int Fans::numberOfFans = 3;

void Fans::init() {
  Log::debug("Initializing fans.");
  Fans::fans[0] = new FanPWM(Pins::FAN_1);
  Fans::fans[1] = new FanPWM(Pins::FAN_2);
  Fans::fans[2] = new FanRelay(Pins::FAN_3);
}

void Fans::defaultSettings() {
  Fans::fans[0]->setSpeed(100);
  Fans::fans[1]->setSpeed(100);
  Fans::fans[2]->setSpeed(100);
}

IFan* Fans::getFan(int fanNumber) {
  Log::debug("Fetching fan");
  if (fanNumber >= 0 && fanNumber < numberOfFans) {
    Log::debug("Fetching fan number: " + fanNumber);
    return Fans::fans[fanNumber];
  }
}

