#ifndef Fans_h
#define Fans_h

#include "IFan.h"

class Fans {

  static int numberOfFans;
  static IFan *fans[3];
    
  public:
    static void init();
    static void defaultSettings();
    static IFan* getFan(int fanNumber);
};

#endif
