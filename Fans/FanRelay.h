#ifndef FanRelay_h
#define FanRelay_h

#include "IFan.h"
#include "Arduino.h"

class FanRelay : public IFan {
  public:
    FanRelay(uint8_t _pin);
    int getSpeed();
    void setSpeed(int _percentage);
  private:
    uint8_t pin;
    bool isActive;
};

#endif
