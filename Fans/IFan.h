#ifndef IFan_h
#define IFan_h

class IFan {
  public:
    virtual ~IFan(){};
    virtual int getSpeed() = 0;
    virtual void setSpeed(int _percentage) = 0;
};

#endif
