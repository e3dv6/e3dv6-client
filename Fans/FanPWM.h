#ifndef FanPWM_h
#define FanPWM_h

#include "IFan.h"
#include "Arduino.h"

class FanPWM : public IFan {
  public:
    FanPWM(uint8_t _outputPin);
    int getSpeed();
    void setSpeed(int _percentage);
  private:
    uint8_t pin;
    int percentage;
};

#endif
