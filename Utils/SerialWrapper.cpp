#include "SerialWrapper.h"
#include "arduino.h"
#include "Log.h"

const int SerialWrapper::okResponse = 'b';
const int SerialWrapper::errorResponse = 'q';

bool SerialWrapper::isSerialAvailable() {
  return Serial.available() == 5;
}

void SerialWrapper::readSerialData() {
  this->serialData[0] = Serial.read();
  delay(100);
  this->serialData[1] = Serial.read();
  delay(100);
  this->serialData[2] = Serial.read();
  delay(100);
  this->serialData[3] = Serial.read();
  delay(100);
  this->serialData[4] = Serial.read();
  delay(100);
  this->serialData[5] = Serial.read();

  Log::debug("Received message: ");
  Log::debug(serialData[0]);
  Log::debug(serialData[1]);
  Log::debug(serialData[2]);
  Log::debug(serialData[3]);
  Log::debug(serialData[4]);
  Log::debug(serialData[5]);
}

void SerialWrapper::sendOkResponse(int id) {
  Log::debug("Sending ok response");

  Serial.write(okResponse);
  Serial.write(id);
  Serial.write('\n');
}

void SerialWrapper::sendOkResponse(int id, int value) {
  Log::debug("Sending ok response with value.");

  Serial.write(okResponse);
  Serial.write(id);
  Serial.print(String(value, HEX));
  Serial.write('\n');
}

void SerialWrapper::sendOkResponse(int id, double value) {
  Log::debug("Sending ok response with value.");

  Serial.write(okResponse);
  Serial.write(id);
  Serial.print(String(value, HEX));
  Serial.write('\n');
}

void SerialWrapper::sendOkResponse(int id, float value) {
  Log::debug("Sending ok response with value.");

  Serial.write(okResponse);
  Serial.write(id);
  Serial.print(String(value, HEX));
  Serial.write('\n');
}

void SerialWrapper::sendErrorResponse(int id) {
    Log::debug("Sending error response");

  Serial.write(errorResponse);
  Serial.write(id);
  Serial.write('\n');
}

void SerialWrapper::sendErrorResponse(int id, int value) {
  Log::debug("Sending error response with value.");

  Serial.write(errorResponse);
  Serial.write(id);
  Serial.print(String(value, HEX));
  Serial.write('\n');
}
