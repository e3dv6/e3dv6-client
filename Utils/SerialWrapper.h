#ifndef SerialWrapper_h
#define SerialWrapper_h

class SerialWrapper {
  static const int okResponse;
  static const int errorResponse;
    
  public:
    int serialData[6];
    bool isSerialAvailable();
    void readSerialData();
    static void sendOkResponse(int id);
    static void sendOkResponse(int id, int value);
    static void sendOkResponse(int id, double value);
    static void sendOkResponse(int id, float value);
    static void sendErrorResponse(int id);
    static void sendErrorResponse(int id, int value);
};

#endif
