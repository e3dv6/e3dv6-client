#ifndef Log_h
#define Log_h

class Log {
  public:
    static void debug(char* message);
    static void debug(const char* message);
    static void debug(int message);

  private:
    static const bool debugMode = false;
};

#endif
