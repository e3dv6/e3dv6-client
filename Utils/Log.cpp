#include "Log.h"
#include "arduino.h"

void Log::debug(char* message) {
  if (Log::debugMode) {
    Serial.println(message);
  }
}

void Log::debug(const char* message) {
  if (Log::debugMode) {
    Serial.println(message);
  }
}

void Log::debug(int message) {
  if (Log::debugMode) {
    Serial.println(message);
  }
}
