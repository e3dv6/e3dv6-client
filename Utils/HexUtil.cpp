#include "HexUtil.h"
#include "Log.h"

int HexUtil::asciiToDec(int data[], int start, int length) {
  int step = 0;
  int result = 0;

  for (int i = start + length - 1; i >= start; i--) {
    int value = HexUtil::asciiToDec(data[i]);

    // Check if value is invalid.
    if (value == -1) {
      return -1;
    }

    value *= pow(16, step);
    result += value;
    step++;
  }

  return result;
}

int HexUtil::asciiToDec(int ascii) {
  if (ascii >= 48 && ascii <= 57) {
    return ascii - 48;
  } else if (ascii >= 97 && ascii <= 102) {
    return ascii - 87;
  } else {
    return -1;
  }
}

// Arduinos own pow function doesn't work (floating point rounding...), so we use our own.
double HexUtil::pow (int x, int n) {
    int i;
    int number = 1;

    for (i = 0; i < n; ++i)
        number *= x;

    return(number);
}
