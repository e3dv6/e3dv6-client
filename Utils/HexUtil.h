#ifndef HexUtil_h
#define HexUtil_h

class HexUtil {
  public:
    static int asciiToDec(int data[], int start, int length);
    static int asciiToDec(int data);
    static double pow(int x, int n);
};

#endif
