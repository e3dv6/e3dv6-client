#include "RequestHandlers/RequestParser.h"
#include "Utils/SerialWrapper.h"
#include "Heater/HeaterManager.h"
#include "Fans/Fans.h"
#include "Utils/Log.h"

int ledPin_3 = 3;

RequestParser parser;
SerialWrapper serial;

void setup() {
  Fans::init();
  Fans::defaultSettings();
  pinMode(ledPin_3, OUTPUT);
  Serial.begin(9600);
  digitalWrite(ledPin_3, HIGH);
  delay(250);
  digitalWrite(ledPin_3, LOW);
  delay(250);
}

void loop() {
    HeaterManager::update();
}

void serialEvent() {
  serial.readSerialData();
  parser.parseCommand(serial.serialData);
}
