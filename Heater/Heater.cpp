#include "Heater.h"
#include "../Utils/Log.h"
#include "../Thermometer/Thermometer.h"
#include "TinkerKit.h"
#include "../Config/Pins.h"

int Heater::targetTemperature = 0;
bool Heater::isHeating = false;
double Heater::heaterPercentage = 0;

TKMosFet mos(Pins::HEATER);         //create the mos object

void Heater::startHeater() {
  Log::debug("Starting Heater");
  Heater::setHeaterPercentage(1);
}

void Heater::stopHeater() {
  Log::debug("Stopping Heater");
  Heater::setHeaterPercentage(0);
}

void Heater::setHeaterPercentage(double percentage) {
  Heater::isHeating = percentage > 0;
  Heater::heaterPercentage = percentage;
  mos.write(TK_MAX * percentage);
}

double Heater::getHeaterPercentage() {
  return Heater::heaterPercentage;
}

bool Heater::isHeaterOn() {
  return Heater::isHeating;
}

void Heater::setTargetTemperature(int temperature) {
  Log::debug("Setting Target Temperature To: ");
  Log::debug(temperature);
  Heater::targetTemperature = temperature;
}

float Heater::getCurrentTemperature() {
  return Thermometer::getTemperature();
}

int Heater::getTargetTemperature() {
  return Heater::targetTemperature;
}
