#ifndef HeaterManager_h
#define HeaterManager_h

class HeaterManager {
    static int temperatureDelta;
    static int ambientTemperature;
    static int decay;
    static double heaterIncrease;
    static int thermalResistance;
    static int future;
    static float tipModel;
    static float heaterModel;

    public:
        static void init();
        static void update();
        static void setDecay(int decay);
        static void setHeaterIncrease(double heaterIncrease);
        static void setThermalResistance(int thermalResistance);
};

#endif
