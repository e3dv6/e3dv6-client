#ifndef Heater_h
#define Heater_h

class Heater {
    static int targetTemperature;
    static bool isHeating;
    static double heaterPercentage;
    
  public:
    static bool isHeaterOn();
    static void setTargetTemperature(int temperature);
    static int getTargetTemperature();
    static float getCurrentTemperature();
    static void startHeater();
    static void stopHeater();
    static void setHeaterPercentage(double percentage);
    static double getHeaterPercentage();
};

#endif
