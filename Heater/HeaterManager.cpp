#include "HeaterManager.h"
#include "Heater.h"
#include "../Utils/Log.h"

int HeaterManager::temperatureDelta = 0.1;
int HeaterManager::ambientTemperature = 25;
int HeaterManager::decay = 380;
double HeaterManager::heaterIncrease = 1.45;
int HeaterManager::thermalResistance = 25;
int HeaterManager::future = 8;

float HeaterManager::tipModel = 0;
float HeaterManager::heaterModel = 0;

void HeaterManager::init() {
    HeaterManager::heaterModel = HeaterManager::tipModel = Heater::getCurrentTemperature();
}

void HeaterManager::update() {
    float targetTemperature = Heater::getTargetTemperature();
    float reading = Heater::getCurrentTemperature();
    float err = reading - HeaterManager::tipModel;

    float heaterModel =  HeaterManager::heaterModel;
    heaterModel = (heaterModel + err) - ((heaterModel + err - HeaterManager::ambientTemperature) / HeaterManager::decay) + (Heater::isHeaterOn() * HeaterManager::heaterIncrease);

    float tipModel = HeaterManager::tipModel;
    tipModel = tipModel + err - ((tipModel + err + HeaterManager::ambientTemperature) / HeaterManager::decay) + ((heaterModel - tipModel + err) / HeaterManager::thermalResistance);

    float tipModelFuture = tipModel - ((tipModel - HeaterManager::ambientTemperature) / HeaterManager::decay * HeaterManager::future) + (((heaterModel - ((heaterModel - HeaterManager::ambientTemperature) / HeaterManager::decay * HeaterManager::future))  - tipModel) / HeaterManager::thermalResistance * HeaterManager::future);

    if (tipModelFuture > targetTemperature + 1) {
        Heater::stopHeater();
    } else if (tipModelFuture < targetTemperature - 1) {
        Heater::startHeater();
    }

    HeaterManager::heaterModel = heaterModel;
    HeaterManager::tipModel = tipModel;
}

void HeaterManager::setDecay(int decay) {
    HeaterManager::decay = decay;
}

void HeaterManager::setHeaterIncrease(double heaterIncrease) {
    HeaterManager::heaterIncrease = heaterIncrease;
}

void HeaterManager::setThermalResistance(int thermalResistance) {
    HeaterManager::thermalResistance = thermalResistance;
}

/*
void HeaterManager::update() {
    float currentTemperature = Heater::getCurrentTemperature();
    float targetTemperature = Heater::getTargetTemperature();
    float temperatureDiff = currentTemperature - targetTemperature;

    if (currentTemperature < targetTemperature - HeaterManager::temperatureDelta) {
        if (temperatureDiff < -30) {
            Heater::setHeaterPercentage(1);
        } else if (temperatureDiff < -20) {
            Heater::setHeaterPercentage(0.5);
        } else {
            Heater::setHeaterPercentage(0.35);
        }
    } else if (targetTemperature == 0) {
        Heater::stopHeater();
    } else if (currentTemperature > targetTemperature + HeaterManager::temperatureDelta) {
        Heater::setHeaterPercentage(0.1);
    }
}
*/