#include "RequestParser.h"
#include "PongRequestHandler.h"
#include "../Utils/Log.h"
#include "../Utils/SerialWrapper.h"
#include "FanRequestHandler.h"
#include "HeaterRequestHandler.h"
#include "../Config/Commands.h"
#include "HeaterManagerRequestHandler.h"

PongRequestHandler pong;
HeaterRequestHandler heaterReqHandler;
FanRequestHandler fanReqHandler;
HeaterManagerRequestHandler heaterManagerReqHandler;

void RequestParser::parseCommand(int data[]) {
  Log::debug("Parsing Command");

  if (isCommand(Commands::PING, data)) {
    pong.sendPong();
  } else if (isCommand(Commands::GET_CURRENT_TEMPERATURE, data)) {
    heaterReqHandler.getCurrentTemperature();
  } else if (isCommand(Commands::GET_TARGET_TEMPERATURE, data)) {
    heaterReqHandler.getTargetTemperature();
  } else if (isCommand(Commands::SET_TARGET_TEMPERATURE, data)) {
    heaterReqHandler.setTargetTemperature(data);
  } else if (isCommand(Commands::IS_HEATER_ON, data)) {
    heaterReqHandler.isHeaterOn();
  } else if (isCommand(Commands::GET_HEATER_PERCENTAGE, data)) {
    heaterReqHandler.getHeaterPercentage();
  } else if (isCommand(Commands::SET_FAN1_SPEED, data)) {
    fanReqHandler.setFan1Speed(data);
  } else if (isCommand(Commands::GET_FAN1_SPEED, data)) {
    fanReqHandler.getFan1Speed();
  } else if (isCommand(Commands::SET_FAN2_SPEED, data)) {
    fanReqHandler.setFan2Speed(data);
  } else if (isCommand(Commands::GET_FAN2_SPEED, data)) {
    fanReqHandler.getFan2Speed();
  } else if (isCommand(Commands::SET_FAN3_SPEED, data)) {
    fanReqHandler.setFan3Speed(data);
  } else if (isCommand(Commands::GET_FAN3_SPEED, data)) {
    fanReqHandler.getFan3Speed();
  } else if (isCommand(Commands::SET_HEATER_DECAY, data)) {
    heaterManagerReqHandler.setDecay(data);
  } else if (isCommand(Commands::SET_HEATER_INCREASE, data)) {
    heaterManagerReqHandler.setHeaterIncrease(data);
  } else if (isCommand(Commands::SET_HEATER_THERMAL_RESISTANCE, data)) {
    heaterManagerReqHandler.setThermalResistance(data);
  } else {
    Log::debug("Unknown Command.");
    SerialWrapper::sendErrorResponse(invalidCommandResponse);
  }
}

bool RequestParser::isCommand(int commandId, int data[]) {
  if (data[0] != commandRequest) {
    return false;
  }

  return (data[1] == commandId);
}
