#ifndef HeaterRequestHandler_h
#define HeaterRequestHandler_h

class HeaterRequestHandler {
  public:
    void isHeaterOn();
    void setTargetTemperature(int data[]);
    void getTargetTemperature();
    void getCurrentTemperature();
    void getHeaterPercentage();
};

#endif
