#include "FanRequestHandler.h"
#include "../Utils/SerialWrapper.h"
#include "../Utils/Log.h"
#include "../Fans/IFan.h"
#include "../Fans/Fans.h"
#include "../Utils/HexUtil.h"
#include "../Config/Commands.h"

void FanRequestHandler::setFan1Speed(int data[]) {
  Log::debug("FanRequestHandler->setFan1Speed");
  int value = HexUtil::asciiToDec(data, 2, 3);
  Fans::getFan(0)->setSpeed(value);
  SerialWrapper::sendOkResponse(Commands::SET_FAN1_SPEED, Fans::getFan(0)->getSpeed());
}

void FanRequestHandler::getFan1Speed() {
  Log::debug("FanRequestHandler->getFan1Speed");
  SerialWrapper::sendOkResponse(Commands::SET_FAN1_SPEED, Fans::getFan(0)->getSpeed());
}

void FanRequestHandler::setFan2Speed(int data[]) {
  Log::debug("FanRequestHandler->setFan2Speed");
  int value = HexUtil::asciiToDec(data, 2, 3);
  Fans::getFan(1)->setSpeed(value);
  SerialWrapper::sendOkResponse(Commands::SET_FAN2_SPEED, Fans::getFan(1)->getSpeed());
}

void FanRequestHandler::getFan2Speed() {
  Log::debug("FanRequestHandler->getFan2Speed");
  SerialWrapper::sendOkResponse(Commands::SET_FAN2_SPEED, Fans::getFan(1)->getSpeed());
}

void FanRequestHandler::setFan3Speed(int data[]) {
  Log::debug("FanRequestHandler->setFan3Speed");
  int value = HexUtil::asciiToDec(data, 2, 3);
  Fans::getFan(2)->setSpeed(value);
  SerialWrapper::sendOkResponse(Commands::SET_FAN3_SPEED, Fans::getFan(2)->getSpeed());
}

void FanRequestHandler::getFan3Speed() {
  Log::debug("FanRequestHandler->getFan3Speed");
  SerialWrapper::sendOkResponse(Commands::SET_FAN3_SPEED, Fans::getFan(2)->getSpeed());
}
