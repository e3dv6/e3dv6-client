#ifndef RequestParser_h
#define RequestParser_h

class RequestParser {
  public:
    void parseCommand(int data[]);

  private:
    int commandRequest = 'a';
    int invalidCommandResponse = 'q';
    
    bool isCommand(int command, int* data);
};

#endif
