#include "PongRequestHandler.h"
#include "../Utils/SerialWrapper.h"
#include "../Utils/Log.h"
#include "../Config/Commands.h"

void PongRequestHandler::sendPong() {
  Log::debug("Sending Pong");
  SerialWrapper::sendOkResponse(Commands::PING);
}
