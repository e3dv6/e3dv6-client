#ifndef HeaterManagerRequestHandler_h
#define HeaterManagerRequestHandler_h

class HeaterManagerRequestHandler {

  public:
    void setDecay(int data[]);
    void setHeaterIncrease(int data[]);
    void setThermalResistance(int data[]);
};

#endif
