#include "HeaterManagerRequestHandler.h"
#include "../Utils/SerialWrapper.h"
#include "../Utils/Log.h"
#include "../Utils/HexUtil.h"
#include "../Config/Commands.h"
#include "../Heater/HeaterManager.h"

void HeaterManagerRequestHandler::setDecay(int data[]) {
  Log::debug("HeaterManagerRequestHandler->setDecay");
  int value = HexUtil::asciiToDec(data, 2, 3);
  HeaterManager::setDecay(value);
  SerialWrapper::sendOkResponse(Commands::SET_HEATER_DECAY);
}

void HeaterManagerRequestHandler::setHeaterIncrease(int data[]) {
  Log::debug("HeaterManagerRequestHandler->setHeaterIncrease");
  double value = HexUtil::asciiToDec(data, 2, 3);
  value *= 0.01;
  HeaterManager::setHeaterIncrease(value);
  SerialWrapper::sendOkResponse(Commands::SET_HEATER_INCREASE);
}

void HeaterManagerRequestHandler::setThermalResistance(int data[]) {
  Log::debug("HeaterManagerRequestHandler->setThermalResistance");
  int value = HexUtil::asciiToDec(data, 2, 3);
  HeaterManager::setThermalResistance(value);
  SerialWrapper::sendOkResponse(Commands::SET_HEATER_THERMAL_RESISTANCE);
}
