#include "HeaterRequestHandler.h"
#include "../Utils/SerialWrapper.h"
#include "../Utils/Log.h"
#include "../Heater/Heater.h"
#include "../Utils/HexUtil.h"
#include "../Config/Commands.h"

void HeaterRequestHandler::isHeaterOn() {
  Log::debug("HeaterRequestHandler->isHeaterOn?");
  SerialWrapper::sendOkResponse(Commands::IS_HEATER_ON, Heater::isHeaterOn());
}

void HeaterRequestHandler::getCurrentTemperature() {
  Log::debug("HeaterRequestHandler->getCurrentTemperature");
  SerialWrapper::sendOkResponse(Commands::GET_CURRENT_TEMPERATURE, Heater::getCurrentTemperature());
}

void HeaterRequestHandler::getTargetTemperature() {
  Log::debug("HeaterRequestHandler->getTargetTemperature");
  SerialWrapper::sendOkResponse(Commands::GET_TARGET_TEMPERATURE, Heater::getTargetTemperature());
}

void HeaterRequestHandler::setTargetTemperature(int data[]) {
  Log::debug("HeaterRequestHandler->setTargetTemperature");
  int value = HexUtil::asciiToDec(data, 2, 3);
  Heater::setTargetTemperature(value);
  SerialWrapper::sendOkResponse(Commands::SET_TARGET_TEMPERATURE, Heater::getTargetTemperature());
}

void HeaterRequestHandler::getHeaterPercentage() {
  Log::debug("HeaterRequestHandler->getHeaterPercentage");
  SerialWrapper::sendOkResponse(Commands::GET_HEATER_PERCENTAGE, Heater::getHeaterPercentage());
}
