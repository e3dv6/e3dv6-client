#ifndef FanRequestHandler_h
#define FanRequestHandler_h

class FanRequestHandler {

  public:
    void setFan1Speed(int data[]);
    void getFan1Speed();
    void setFan2Speed(int data[]);
    void getFan2Speed();
    void setFan3Speed(int data[]);
    void getFan3Speed();
};

#endif
